/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

// @ts-ignore
import React from "react"

import Header from "./header"
import "./layout.css"

const Layout: React.FC = ({
    children,
}) => {
  return (
    <>
        <Header />
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.0875rem 1.45rem`,
        }}
      >
        <main>{children}</main>
        <footer>
          © {new Date().getFullYear()}, Built with
          {` `}
          <a target="_blank" href="https://www.gatsbyjs.org">Gatsby</a>
        </footer>
      </div>
    </>
  )
}


export default Layout
